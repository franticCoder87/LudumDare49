extends RigidBody2D

signal block2_collision_paddle

func _on_Block_2_body_entered(body): # when a block collides with the paddle
	if body.get_name() == "Paddle":
		emit_signal("block2_collision_paddle")
		queue_free()
