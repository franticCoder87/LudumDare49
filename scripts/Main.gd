extends Node2D

signal level_select(level_index) # called when choosing a new level

export var score_multiplier = 10
export var speed = 500
export var lives = 3
var ball_scene = preload("res://scenes/Ball.tscn")
var ball
var ball_dir = [1, 2]
var block_2_scene = preload("res://scenes/Block_2.tscn")
var screen_size
var current_level = 0
var score = 0
var paddle_w = 128
var paddle_h = 8
var shift_movement_left
var shift_movement_right
var total_movements = 0
var max_movements = 5

func _ready():
	screen_size = get_viewport_rect().size # get the screen coords
	
	$Paddle.position = Vector2(screen_size.x / 2, screen_size.y - 64)
	$Paddle.connect("block2_collision_paddle", self, "_on_block2_collision_paddle")
	
	ball = ball_scene.instance() # create a new ball instance
	ball.position = Vector2(400, 300)
	add_child(ball)
	ball.get_child(0).connect("body_entered", self, "_on_Ball_body_entered") # ball collide event
	
	$UI/TxtLives.text = "Lives: " + str(lives) # update life text
	
	emit_signal("level_select", current_level) # load the first level
	
func _process(delta):
	if Input.is_key_pressed(KEY_LEFT) and $Paddle.position.x > paddle_w / 2: # move the paddle left
		$Paddle.position.x -= speed * delta
	if Input.is_key_pressed(KEY_RIGHT) and $Paddle.position.x < screen_size.x - paddle_w / 2: # move the paddle right
		$Paddle.position.x += speed * delta
		
	$Paddle.position.x = clamp($Paddle.position.x, 0, screen_size.x)

func _physics_process(delta): # maintain a constant pace
	var ball_velocity = ball.get_child(0).get_linear_velocity()

	if ball_velocity.x > 150: # too fast on x
		ball.get_child(0).set_linear_velocity(Vector2(150, ball_velocity.y))
	if ball_velocity.y > 150: # too fast on y
		ball.get_child(0).set_linear_velocity(Vector2(ball_velocity.x, 150))

#	var int_angle = int(rad2deg(ball_velocity.angle()))
#	if int_angle == 45 and ball_velocity.x != ball_velocity.y: # moving southeast
#		ball.get_child(0).set_linear_velocity(Vector2(150, 150))
#	if int_angle == -45 and ball_velocity != Vector2(150, -150): # moving northeast
#		ball.get_child(0).set_linear_velocity(Vector2(150, -150))
#	if int_angle == -135 and ball_velocity != Vector2(-150, -150): # moving northwest
#		ball.get_child(0).set_linear_velocity(Vector2(-150, -150))
#	if int_angle == 135 and ball_velocity != Vector2(-150, 150): # moving southwest
#		ball.get_child(0).set_linear_velocity(Vector2(-150, 150))

func _on_Ball_body_entered(body): # when there's a collision with the ball
	if body.get_name() == "Block": # destroy the block and update the score
		body.get_parent().remove_from_group("Blocks")
		body.queue_free()
		
		score += score_multiplier # update the player's score
		$UI/TxtScore.text = "Score: " + str(score)
		
		$Audio/BlockHit.play() # play a sound
		
		if get_tree().get_nodes_in_group("Blocks").size() <= 0: # load next level
			current_level += 1
			emit_signal("level_select", current_level)
			
	elif body.get_name() == "Paddle":
		$Audio/PaddleHit.play() # play a sound

func _on_Wall_Lower_ball_destroyed(): # when the ball is lost and is destroyed
	if lives > 0: # if we're still in the game
		lives -= 1
		$UI/TxtLives.text = "Lives: " + str(lives)
		$Audio/LostBallFx.play()
		$UI/ProjectileTimer.set_wait_time(20)
	else: # game over scenario
		ball.queue_free()
		$Paddle.queue_free()
		get_tree().change_scene("res://scenes/GameOver.tscn")

func _on_block2_collision_paddle():
	print("paddle collided with block 2")

func _on_StartTimer_timeout(): # when the start timer reaches 0
	ball.get_child(0).set_linear_velocity(Vector2(150, 150))
	#$UI/ProjectileTimer.start() # delay when projectiles begin falling

func _on_ProjectileTimer_timeout(): # when projectiles begin to fill
	total_movements = 1
		
	$Audio/ProjectilesFx.play() # play a sound
	$UI/ProjectileTimer.set_wait_time(20) # restart the timer
