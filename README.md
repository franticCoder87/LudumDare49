# LudumDare49
A simple game of Breakout... but it's not!
## To-Do List
* Change linear velocity when ball collides with paddle
* Incrementally increase ball speed as game goes on
* Add powerups/debuffs for ball and paddle
* Add new types of blocks
* Add more levels
* Add background images
