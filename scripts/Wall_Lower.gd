extends Area2D

signal ball_destroyed

func _on_Wall_Lower_body_entered(body): # when the ball collides with lower wall
	if body.get_name() == "Ball":
		emit_signal("ball_destroyed")
