extends RigidBody2D

var screen_size
var ball_dir = [1, 2]
var ball_h = 16

func _ready():
	screen_size = get_viewport_rect().size

func _integrate_forces(state):
	var state_transform = state.get_transform()
	
	if state_transform.origin.y >= screen_size.y + (ball_h * 3):
		state_transform.origin = Vector2(400, 300)	
		state.set_linear_velocity(Vector2(0, 0))	
		state.set_transform(state_transform)
		
		get_node("../StartTimer").start() # start the timer

func _on_StartTimer_timeout(): # timed delay before ball starts moving again
	var dir = ball_dir[randi() % ball_dir.size()] # set linear velocity
	if dir == 1:
		set_linear_velocity(Vector2(-150, 150))
	elif dir == 2:
		set_linear_velocity(Vector2(150, 150))
