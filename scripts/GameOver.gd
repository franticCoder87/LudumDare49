extends Node2D

var fadeIn
var fadeOut

func _ready():
	fadeIn = get_node("UI/TxtNote/FadeIn") # fade in effect
	_define_fadeIn_tween()
	
	fadeOut = get_node("UI/TxtNote/FadeOut") # fade out effect
	_define_fadeOut_tween()
	
	fadeOut.start()

func _process(delta):
	if Input.is_action_just_pressed("restart_game"):
		get_tree().change_scene("res://scenes/MainMenu.tscn")

func _on_FadeIn_tween_completed(object, key):
	_define_fadeOut_tween()
	fadeOut.start()

func _on_FadeOut_tween_completed(object, key):
	_define_fadeIn_tween()
	fadeIn.start()

func _define_fadeIn_tween():
	fadeIn.interpolate_property($UI/TxtNote, "custom_colors/font_color",
		Color(1, 1, 1, 0), Color(1, 1, 1, 1), 1, 
		Tween.TRANS_LINEAR, Tween.EASE_IN)
	
func _define_fadeOut_tween():
	fadeOut.interpolate_property($UI/TxtNote, "custom_colors/font_color",
		Color(1, 1, 1, 1), Color(1, 1, 1, 0), 1, 
		Tween.TRANS_LINEAR, Tween.EASE_IN)
